// q1
select customer.customer_id, first_name, last_name from customer
inner join payment on customer.customer_id = payment.customer_id;

// q2
select rental.rental_id, customer.first_name, customer.last_name from customer
inner join rental on rental.rental_id = customer.customer_id;
